#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    width = 640; height = 480;
    grabber.setDeviceID(0);
    grabber.setVerbose(true);
    grabber.initGrabber(width, height);


    colorImg.allocate(width, height);
    grayImage.allocate(width, height);
    grayBg.allocate(width, height);
    grayDiff.allocate(width, height);

    bLearnBackground = true;

    settings.load("settings.xml");

    baudrate = settings.getValue("baudrate", 1180);
    port = settings.getValue("portname", "/dev/unknown");
    serial.setup(settings.getValue("portname", ""), settings.getValue("baudrate", 1180));

    gui.setup();
    gui.add(threshold.setup("threshold", settings.getValue("threshold", 70), 0, 220));
    gui.add(minSize.setup("minSize", settings.getValue("minSize", 0.01), 0, 1));
    gui.add(maxSize.setup("maxSize", settings.getValue("maxSize", 0.5), 0, 1));
    gui.add(leftH.setup("left",
      settings.getValue("left", 0.5), 0, 1));
    gui.add(rightH.setup("right",
      settings.getValue("right", 0.5), 0, 1));
    gui.add(hh.setup("line width",
       settings.getValue("linewidth", 0.1), 0, 1));
    gui.add(fadeSpeed.setup("fadespeed",
                     settings.getValue("fadespeed", 0.5), 0, 1));
    gui.add(MP.setup("handspeed",
                     settings.getValue("handspeed", 5), 0, 255));
    gui.add(D.setup("stopvalue",
                     settings.getValue("stopvalue", 100), 0, 255));
    gui.add(DelayToBeginAgain.setup("delay_to_begin_again",
                    settings.getValue("delay_to_begin_again", 5), 0, 10));

    gui.setPosition(ofGetWidth() / 2, 100);

    log_length = 24;

    timeWhenMRCalled = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
void ofApp::update(){
    grabber.update();

    if ((!initialBackgroundLearned) && (ofGetElapsedTimef() > 2)) {
        initialBackgroundLearned = true;
        bLearnBackground = false;
    }

    if (serial.available() > 0) {
        if (serial.available() > 100) serial.flush();
        dat4ik = serial.readByte();
        logString(ofToString(dat4ik));
    }

    bool bNewFrame = false;

    bNewFrame = grabber.isFrameNew();
    if (bNewFrame){

        colorImg.setFromPixels(grabber.getPixels(), 640,480);
        grayImage = colorImg;
        if (bLearnBackground == true){
            grayBg = grayImage;		// the = sign copys the pixels from grayImage into grayBg (operator overloading)
            bLearnBackground = false;
        }

        // take the abs value of the difference between background and incoming and then threshold:
        grayDiff.absDiff(grayBg, grayImage);
        grayDiff.threshold(threshold);

        // find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
        // also, find holes is set to true so we will get interior contours as well....
        contourFinder.findContours(grayDiff, /*140*/minSize * (640*480), /*(640*480)/(1.3)*/ maxSize * (640*480), 10, false, true);

    }

    if (fired > 0.05) {
        fired -= fired / (70 * (1 - fadeSpeed));
    } else if (fired > 0) {
            ofLog() << "Stopped at " <<  fired << "!";
            fired = 0;
            sendCommand("MR");
    }

    if (fired == 0) {
        if (ofRandom(100) > 90) sendCommand("MF", 0);
        //if (ofRandom(100) > 90) sendCommand("MP", MP);
    }

    if (log.size() > log_length) log.erase(log.begin());
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(0);
    float scale = (float)grabber.width / (float)ofGetWidth();
    ofPushMatrix();
    ofScale(1/scale/2, 1/scale/2);

    ofSetColor(255, 50);
    grabber.draw(0, 0);

    ofSetColor(255, 220);
    //grabber.draw(0, 0, grabber.width, grabber.height);
    grayDiff.draw(0,0, grabber.width, grabber.height);


    ofSetColor(255);
    ofSetLineWidth(3);
//    ofLine(0, height * leftH,
//           width, height * rightH);

    ofSetColor(255, 100);
    ofLine(0, height * leftH - hh * height,
           width, height * rightH - hh * height);
    ofLine(0, height * leftH + hh * height,
           width, height * rightH + hh * height);
    ofFill();
    ofBeginShape();
        ofVertex(0, height * leftH - hh * height);
        ofVertex(width, height * rightH - hh * height);
        ofVertex(width, height * rightH + hh * height);
        ofVertex(0, height * leftH + hh * height);
    ofEndShape();

    ofSetLineWidth(2);

    int candidate = -1;
    float candidateX = -1;
    for (int i = 0; i < contourFinder.nBlobs; i++){
//        contourFinder.blobs[i].draw(0, 0);
        ofSetColor(255,0,0,130);
        ofRect(contourFinder.blobs[i].boundingRect);
        ofSetColor(255);
        ofLine( contourFinder.blobs[i].boundingRect.getTopLeft(),
               contourFinder.blobs[i].boundingRect.getBottomRight());
        ofLine( contourFinder.blobs[i].boundingRect.getTopRight(),
               contourFinder.blobs[i].boundingRect.getBottomLeft());

        float xx = contourFinder.blobs[i].boundingRect.getTopRight().x;
        if (xx > candidateX) {
            candidateX = xx;
            candidate = i;
        }
    }
    if (candidate > -1) {
        ofSetColor(0, 255, 0, 200);
        ofRect(contourFinder.blobs[candidate].boundingRect);

        ofPoint p = contourFinder.blobs[candidate].boundingRect.getTopRight();
        ofPoint pNormalized = p / ofPoint(width, height);
        float lerpedLineAtX = ofLerp(leftH, rightH, pNormalized.x);
        if ((pNormalized.y > lerpedLineAtX - hh) && (pNormalized.y < lerpedLineAtX + hh)) {
            if ((fired == 0) && (ofGetElapsedTimeMillis() - timeWhenMRCalled > (DelayToBeginAgain * 1000))) {
                fire();
            }
        } else if ((fired > 0) && (!ignoreBlobsTillNextFire)) {
            sendCommand("MR"); // safety first!
        }
    }

//    ofSe1tColor(255);
//    ofDrawBitmapString("Got " + ofToString(contourFinder.blobs.size()) + " blobs", 50, 50);

    ofPushMatrix();
        ofTranslate((scale) * ofGetWidth(), 0);
        ofSetColor(255);
        grabber.draw(0,0);
    ofPopMatrix();

    ofPopMatrix();

    ofSetColor(255, 60);
    ofRect(0, ofGetHeight() / 2, ofGetWidth(), 70);
    ofSetColor(255, fired * 255);
    ofRect(0, ofGetHeight() / 2, ofGetWidth() * (1 - fired), 70);
    gui.draw();

    for (int i = 0; i < log.size(); i++) {
        ofSetColor(255);
        ofDrawBitmapString(log[i], 0, ofGetHeight()/2 + 80 + i * 11);
    }

    ofSetColor(255, 0, 0);
    ofDrawBitmapString(ofToString(dat4ik), 20, 20);
    ofDrawBitmapString("Symbols left:" + ofToString(serial.available()), 100, 20);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    ofLog() << key;
    if ((key < 57) && (key >= 49)) threshold = (57 - key) * 10;
    switch (key){
        case ' ':
            bLearnBackground = true;
            break;
        case 'f':
            fire();
            ignoreBlobsTillNextFire = true;
            break;
        case 's':
            settings.setValue("threshold", threshold);
            settings.setValue("minSize", minSize);
            settings.setValue("maxSize", maxSize);
            settings.setValue("left", leftH);
            settings.setValue("right", rightH);
            settings.setValue("linewidth", hh);
            settings.setValue("handspeed", MP);
            settings.setValue("stopvalue", D);
            settings.setValue("fadespeed", fadeSpeed);
            settings.setValue("baudrate", baudrate);
            settings.setValue("portname", port);
            settings.setValue("delay_to_begin_again", DelayToBeginAgain);
            settings.save("settings.xml");
            break;
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
