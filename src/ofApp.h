#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxGui.h"
#include "ofxXmlSettings.h"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

    int width, height;

    ofVideoGrabber grabber;

    ofxCvColorImage			colorImg;

    ofxCvGrayscaleImage 	grayImage;
    ofxCvGrayscaleImage 	grayBg;
    ofxCvGrayscaleImage 	grayDiff;

    ofxCvContourFinder 	contourFinder;

    bool bLearnBackground;
    bool initialBackgroundLearned;

    ofSerial serial;
    int baudrate; string port;

    ofxIntSlider threshold;
    ofxFloatSlider minSize, maxSize;
    ofxFloatSlider leftH, rightH, hh, fadeSpeed;
    ofxIntSlider MP, D, DelayToBeginAgain;

    double timeWhenMRCalled;

    ofxGuiGroup gui;

    int log_length;

    bool ignoreBlobsTillNextFire;

    void logString(string arg) {
        log.push_back(ofToString(ofGetElapsedTimef()) + " " + arg);
        ofLog() << ofToString(ofGetElapsedTimef()) << " "  <<  arg;
    }

    void sendCommand(string prefix, int arg = -1) {
        if (prefix == "MF") {
            unsigned char command[2] = {'M', 'F'};
            serial.writeBytes(command, 2);
            logString("Sent MF to arduino.");
        }
        if (prefix == "MR") {
            unsigned char command[2] = {'M', 'R'};
            serial.writeBytes(command, 2);
            logString("Sent MR to arduino.");
            timeWhenMRCalled = ofGetElapsedTimeMillis();
            fired = 0;
        }
        if (prefix == "MP") {
            char argChar = (char)arg;
            unsigned char command[3] = {'M', 'P', argChar};
            serial.writeBytes(command, 3);
            int dInt = D;
            argChar = dInt;
            unsigned char command2[2] = {'D', argChar};
            serial.writeBytes(command2, 2);
            logString("Sent MP to arduino.");
        }
        if (prefix == "S") {
            unsigned char command[1] = {'S'};
            serial.writeBytes(command, 1);
            logString("Sent S to arduino.");
        }
    }

    uint8_t dat4ik;

    vector<string> log;

    void fire() {
        fired = 1;
        ignoreBlobsTillNextFire = false;
        sendCommand("MP", MP);
        sendCommand("S");
    }
    float fired;

    ofxXmlSettings settings;
};
